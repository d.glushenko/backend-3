CREATE TABLE form (
    id_person SERIAL,
    first_name CHAR(100),
    email CHAR(100),
    nowdate DATE,
    sex CHAR(20),
    kolvo INTEGER,
    abilities TEXT,
    bio TEXT,
    checkbox BOOLEAN
);
